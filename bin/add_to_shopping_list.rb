#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/cookidoo_parser'

arg = ARGV.join(' ')
arg = 'ping from container' if arg.empty?

CookidooParser.from_file.add_to_shopping_list(arg).quit
puts(arg, 'added!')
